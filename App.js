import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

export default class App extends React.Component {
    state = {
        result: '',
    };

    buttonPress = button => {
        let oldButton = this.state.result,
            lastButton;
        if (oldButton === 0) {
            oldButton = '';
        }

        switch (button) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.setState({
                    result: oldButton + button
                });
                break;
            case "+":
            case "-":
            case "*":
            case "/":
                lastButton = oldButton.slice(-1);
                if (lastButton === '+' || lastButton === '-' || lastButton === '*' || lastButton === '/')
                    this.setState({
                        result: oldButton.slice(0, -1) + button
                    });
                else
                    this.setState({
                        result: oldButton + button
                    });
                break;
            case '=':
                try {
                    this.setState({
                        result: eval(this.state.result) + ''
                    });
                } catch (e) {
                    this.setState({result: 'NaN'});
                }
                break;
            case '.':
                lastButton = oldButton.slice(-1);
                if (lastButton !== '.') {
                    this.setState({
                        result: oldButton + button
                    });
                }
                break;
            case '&lt;':
                const window = this.state.result.split('');
                window.pop();
                this.setState({result: window.join('')})
        }
    };

    refresh = ( ) => {
        this.setState({result: ''});
    };

  render() {
      let {result} = this.state;
      return (
        <View style={styles.container}>
          <View style={styles.display}>
              <Text style={styles.result}>
                  {result}
              </Text>
          </View>
          <View style={styles.input}>
              <View style={styles.row}>
                  <TouchableOpacity  style={styles.opacity} onPress={() => this.refresh()}>
                      <Text style={styles.delete}>
                          AC
                      </Text>
                  </TouchableOpacity>
                  <TouchableOpacity  style={styles.opacity} onPress={() => this.buttonPress('&lt;')}>
                      <Text style={styles.inputText}>
                          &lt;
                      </Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.row}>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('+')}>
                      <Text style={styles.item}>+</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('-')}>
                      <Text style={styles.item}>-</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('*')}>
                      <Text style={styles.item}>*</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('/')}>
                      <Text style={styles.item}>/</Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.row}>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('7')}>
                      <Text style={styles.item}>7</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('8')}>
                      <Text style={styles.item}>8</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('9')}>
                      <Text style={styles.item}>9</Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.row}>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('4')}>
                      <Text style={styles.item}>4</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('5')}>
                      <Text style={styles.item}>5</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('6')}>
                      <Text style={styles.item}>6</Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.row}>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('1')}>
                      <Text style={styles.item}>1</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('2')}>
                      <Text style={styles.item}>2</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('3')}>
                      <Text style={styles.item}>3</Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.row}>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('.')}>
                      <Text style={styles.item}>.</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('0')}>
                      <Text style={styles.item}>0</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.opacity} onPress={() => this.buttonPress('=')}>
                      <Text style={styles.item}>=</Text>
                  </TouchableOpacity>
              </View>
          </View>
        </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  display: {
    flex: 2,
    backgroundColor: '#193441'
  },
  input: {
    flex: 8,
    backgroundColor: '#3E606F'
  },
    result: {
        color: '#fff',
        fontSize: 60,
        padding: 25,
        textAlign: 'right'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#3E606F',
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    inputText: {
        color: 'rgba(255, 255, 255, 0.9)',
        fontSize: 23,
        padding: 5
    },
    delete: {
        color: 'rgba(255, 255, 255, 0.5)',
        fontSize: 23,
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    opacity: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        margin: 1,
    },
    item: {
        color: '#fff',
        fontSize: 26,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
});
